`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	output           trap,
	output reg [7:0] out_byte,
	output reg [7:0] anodo,
	output reg [7:0] catodo,
	output reg       enable);

	reg [3:0] unidades;
	reg [3:0] decenas;
	reg [3:0] centenas;
	wire [7:0] segmentos_dig0, segmentos_dig1, segmentos_dig2; 
	reg [14:0] contador;
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;

	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			enable <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				enable <= 1;
				out_byte <= mem_la_wdata;
			end
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			enable <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					enable <= 1;
					out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate

	integer i;
	always @(out_byte) begin
		centenas = 0;
		decenas = 0;
		unidades = 0;
		for ( i = 7; i>=0 ; i = i-1) begin
			
			if (centenas >= 5) begin
				centenas = centenas + 3;
			end
			if (decenas >= 5) begin
				decenas = decenas + 3;
			end
			if (unidades >= 5) begin
				unidades = unidades + 3;
			end

			//Shift
			centenas = centenas << 1;
			centenas[0] = decenas[3];
			decenas = decenas << 1;
			decenas[0] = unidades[3];
			unidades = unidades << 1;
			unidades[0] = out_byte[i];
		end
	end

	BCDseg primerDigito(
		.bcd (unidades),
		.seg (segmentos_dig0)
	);

	BCDseg segundoDigito(
		.bcd (decenas),
		.seg (segmentos_dig1)
	);
	BCDseg tercerDigito(
		.bcd (centenas),
		.seg (segmentos_dig2)
	);
	
	always @(posedge clk ) begin
		if (resetn == 0) begin
			contador <= 0;
			anodo <= 8'b11111000; 
			catodo <= 8'b00000011;
		end else begin
			
			//unidades
			if (contador == 10923) begin
				catodo <= segmentos_dig0;
				anodo <= 8'b11111110;
				contador <= contador + 1;
			//decenas	
			end else if (contador == 21845) begin
				catodo <= segmentos_dig1;
				anodo <= 8'b11111101;
				contador <= contador + 1;

			//centenas	
			end else if (contador == 32767) begin
				catodo <= segmentos_dig2;
				anodo <= 8'b11111011;
				contador <= contador + 1;
			end else begin 
				contador <= contador + 1;
			end
		end
	end
	
endmodule
