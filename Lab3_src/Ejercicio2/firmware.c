#include <stdint.h>

static void putuint(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}

static void putuint1(uint32_t i) {
	*((volatile uint32_t *)0x10000004) = i;
}

static void putuint2(uint32_t i) {
	*((volatile uint32_t *)0x10000008) = i;
}



void main() {
	uint32_t pixel_position = 0;
	uint32_t pixel_color = 0;
	uint32_t control_register = 0;
	uint32_t red =0b111100000000;
	uint32_t blue =0b000011110000;
	uint32_t green = 0b000000001111;
	uint32_t purple =0b111111110000;
	uint32_t black =0b000000000000;
	uint32_t white =0b111111111111;
	uint32_t height =7;
	uint32_t width =7;
	uint32_t row =0;
	uint32_t column =0; 
	uint32_t enable =1;
	uint32_t contador =0;
	uint32_t contadorF =0;
	uint32_t contadorC =0;

	while (1) {		
		// contador ++;
		control_register = 0;		
		pixel_position = column + (row<< 16);
		putuint(pixel_position);				
		if (enable==1){
			column++;
			contadorC++;
			
		}		
		switch(contador%2)
			{
				case 0: 
					pixel_color = white;
					if (enable == 1){
						control_register =1;
					}				
					putuint2(control_register);
				break;
				case 1: 
					pixel_color = black;
					if (enable == 1){
						control_register =1;
					}
					putuint2(control_register);
				break;				
				default: 
				break;
			}
		contador ++;
		if(row == 8 && column == 8){
			enable = 0;	
			// contador ++;
			control_register = 0;
			putuint2(control_register);		
		}
		putuint1(pixel_color);		
		if ( (column) == 8 && enable == 1){
			row++;
			// contador ++;
			contadorF++;
			column = 0;
		}		
	}
}
