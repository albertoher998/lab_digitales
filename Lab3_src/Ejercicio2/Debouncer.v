module Debouncer(
    input clk25M,
    input ButtonD,
    output reg byte,

    output reg ButtonOut
);    
    reg ButtonF1, ButtonF2, ButtonF3, enable, ande;
	reg [21:0] cont;
	// assign reset_cont = ButtonF1^ButtonF2;
    always@(posedge clk25M)begin
		ButtonOut <= 0;
        byte <= 0;
    	ButtonF1 <= ButtonD;
    	ButtonF2 <= ButtonF1;
        ButtonF3 <= ButtonF2;
        ande <= ButtonF1 & ButtonF2 & ButtonF3;
        if(ande == 1)begin
	        enable <= 1;
        end
		if(cont == 4000000 && enable == 1)begin
	    	ButtonOut <= ande;
            enable <= 0;
            byte <= 1;	 
            cont <= 0;		
      	end else begin
	     	cont <= cont + 1;
      	end
      
    end
endmodule