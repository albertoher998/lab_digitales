module boardAccsess(
    input [4:0] button,
    input clk,
    input resetn,

    output reg [5:0]posicion,
    output reg [11:0]rojo
);

    reg [2:0] filas, columnas;
    wire neg;
    assign neg = ~(button[0]|button[1]|button[2]|button[3]|button[4]);
    always @(negedge neg) begin
        if (resetn == 0) begin
            filas <= 3;
            columnas <= 4;
            rojo <= 'hF00;
            posicion <= 28;
        end else begin
            posicion <= 8*filas + columnas;
            if (button[0] == 1) begin
                filas <= filas - 1;
            end
            if (button[1] == 1) begin
                filas <= filas + 1;
            end
            if (button[2] == 1) begin
                columnas <= columnas - 1;
            end
            if (button[3] == 1) begin
                columnas <= columnas + 1;
            end
            if (button[4] == 1) begin
                if (rojo == 'hF00)begin
                    rojo <= 'h0F0;
                end
                if (rojo == 'h0F0) begin
                    rojo <= 'h00F;
                end
                if (rojo == 'h00F) begin
                    rojo <= 'hF00;
                end
            end
        end
    end
endmodule 