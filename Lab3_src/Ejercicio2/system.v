`timescale 1 ns / 1 ps

module system (
	input clk,
	input resetn,
	input [4:0] Button,
	output trap,
	output  [11:0] color,
	output  Hsync,
	output  Vsync,
	output reg enable,
	output [4:0] out_byte);
    
	wire clk25M;
	reg control;
	reg [11:0] RGB;	
	reg [31:0] posicion;
	
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;

	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin			
			mem_ready <= 1;
			enable <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h10000000) begin
				enable <= 1;
				posicion <= mem_la_wdata;
			end
			if (mem_la_write && mem_la_addr == 32'h10000004) begin
				enable <= 1;
				RGB <= mem_la_wdata;
			end
			if (mem_la_write && mem_la_addr == 32'h10000008) begin
				enable <= 1;
				control <= mem_la_wdata;
			end		
			// if(mem_la_read && mem_la_addr == 32'h0FFF_FFF8)begin
	    	// 	mem_rdata <= multiplicacion16b;
			// end	
			// if(mem_la_read && mem_la_addr == 32'h0FFF_FFFC)begin
	    	// 	mem_rdata <= listo;
	 		// end	
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			enable <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					enable <= 1;
					// out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate

	newClk clock_25(
		.clk (clk),
		.resetn (resetn),
		.clk25M (clk25M)
	);
	wire [4:0] ButtonOut;	
	Debouncer BounceAr(
		.clk25M (clk25M),
		.ButtonD (Button[0]),
		.byte (out_byte[0]),
		.ButtonOut (ButtonOut[0])
	);
	Debouncer BounceAb(
		.clk25M (clk25M),
		.ButtonD (Button[1]),
		.byte (out_byte[1]),
		.ButtonOut (ButtonOut[1])
	);
	Debouncer BounceIz(
		.clk25M (clk25M),
		.ButtonD (Button[2]),
		.byte (out_byte[2]),
		.ButtonOut (ButtonOut[2])
	);
	Debouncer BounceDe(
		.clk25M (clk25M),
		.ButtonD (Button[3]),
		.byte (out_byte[3]),
		.ButtonOut (ButtonOut[3])
	);
	Debouncer BounceC(
		.clk25M (clk25M),
		.ButtonD (Button[4]),
		.byte (out_byte[4]),
		.ButtonOut (ButtonOut[4])
	);
	goodVGAaccsess VGA_(
		.clk (clk25M),
		.clk2 (clk),
		.resetn (resetn),
		.control (control),
		.posicion (posicion),
		.RGB (RGB),
		.button (ButtonOut),

		.Hsync (Hsync),
		.Vsync (Vsync),
		.colorOut (color)
	);
endmodule
	
	