module DebouncerTec(
    input clk25M,
    input [31:0]ButtonD,    

    output reg [31:0] ButtonOut
);    
    reg [31:0] ButtonF1, ButtonF2, ButtonF3, ButtonF4, ButtonF5, ButtonF6, ande;
    reg enable;
	reg [21:0] cont;
	// assign reset_cont = ButtonF1^ButtonF2;
    always@(posedge clk25M)begin
		ButtonOut <= 0;
        
        
        ButtonF1 <= ButtonD;       
        ButtonF2 <= ButtonF1;
        ButtonF3 <= ButtonF2;
        ButtonF4 <= ButtonF3;
        ButtonF5 <= ButtonF4;
        ButtonF6 <= ButtonF5;
        
        
    	
    	
       
		if(cont == 4194303)begin
	    	ButtonOut <= ButtonF6;
            enable <= 0;            	 
            cont <= 0;		
      	end else begin
	     	cont <= cont + 1;
      	end
      
    end
endmodule