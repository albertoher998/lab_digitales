module accsesVGA(
    input clk;
    input resetn,
    input control,
    input [31:0] posicion,
    input RGB,

    output reg Hsync,
    output reg Vsync,
    output reg [11:0]colorOut    
);
reg colorMemory [11:0];
reg lectura [5:0];
reg [5:0] posicionProce;
wire [2:0] filas, columnas;

assign columnas = posicion[2:0];
assign filas = posicion[18:16];

always @(*) begin
    if (resetn == 0) begin
        posicionProce = 0;
    end
    posicionProce = filas * 8 + columnas;
end
integer i;
always @(posedge clk) begin
    if (resetn == 0) begin
        lectura <= 0;
    end else begin
        while (cHsync != 521) begin
            bloque <= 0;
            for (i = 0; i < 800; i = i + 1) begin
                cHsync <= cHsync + 1;
                if (i < 96) begin
                    Hsync <= 0;
                    colorOut <= 0;
                end else if (i >= 96 && i < 144) begin
                    Hsync <= 1;
                    colorOut <= 0;
                end else if (i >= 144 && i < 784) begin                
                    if (bloque % 80 == 0) begin
                        lectura <= lectura + 1;
                    end
                    colorOut <= colorMemory;
                    bloque <= bloque + 1;
                end else if (i >= 784 && i < 800) begin
                    colorOut <= 0;
                end
            end
            if (cHsync < 2) begin //TODO arreglar la sobre escritura, jugar con senal de enable.
                colorOut <= 0;
                Vsync <= 0;
            end else if (cHsync >= 2 && cHsync < 31) begin
                Vsync <= 1;
                colorOut <= 0;
            end else if (cHsync >= 31 && cHsync < 511) begin
                colorOut <= colorMemory;
            end else if (cHsync >= 511 && cHsync < 521) begin
                colorOut <= 0;
            end
        end
    end
   
end

RAM_SINGLE_READ_PORT # (12, 6, 8*8) VideoMemory(
    .Clock (clk),
    .iWriteEnable (control),
    .iReadAddress (lectura),
    .iWriteAddress (posicionProce),//2^6 6bits
    .iDataIn (RGB)
    .oDataOut (colorMemory)
);