module newClk(
    input clk,
    input resetn,
    output reg clk25M
);
// reg [1:0] counter;
reg clockM;
always @(posedge clk) begin
    if (resetn == 0) begin
        clockM <= 1;              
    end else begin      
        clockM <= ~clockM;
    end
end

always @(posedge clockM) begin
    if (resetn == 0) begin
        clk25M <= 1;              
    end else begin      
        clk25M <= ~clk25M;
    end
end

endmodule // newCLK

