module goodVGAaccsess(
    input clk,
    input clk2,
    input resetn,
    input control,
    input [31:0] posicion,
    input [11:0]RGB,

    output reg Hsync,
    output reg Vsync,
    output reg [11:0]colorOut    
);

reg [5:0] lectura, cambioColor;
reg [9:0] CuentaFilas;
reg [5:0] posicionProce;
wire [2:0] filas, columnas;
reg [2:0] cuentaBloque;
reg [9:0] cVsync, cHsync, bloque;
wire [11:0] colorMemory ;
reg enableFilas;

assign columnas = posicion[2:0];
assign filas = posicion[18:16];

always @(*) begin
    if (resetn == 0) begin
        posicionProce = 0;
    end
    posicionProce = filas * 8 + columnas;
end
always @(posedge clk ) begin
    if (resetn == 0) begin
        CuentaFilas <= 0;
        cuentaBloque <= 0;
        cambioColor <= 0;
        colorOut <= 0;
        enableFilas <= 0;
        lectura <= 0;
        bloque <=0;
        cHsync <= 0;
        cVsync <= 0;
        Hsync <= 0;
        Vsync <= 0;
    end else begin 
        cHsync <= cHsync + 1;
        if (cVsync < 2) begin 
            CuentaFilas <= 0;
            cambioColor <= 0;
            
            Vsync <=0;           
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync < 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 799)begin
                colorOut <= 0;
                cHsync <= 0;
                cVsync <= cVsync + 1;
            end            
        end else if (cVsync >= 2 && cVsync < 31) begin  
            Vsync <= 1;          
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync < 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 799)begin
                colorOut <= 0;
                cHsync <= 0;
                cVsync <= cVsync + 1;
            end            
        end else if (cVsync >= 31 && cVsync < 511) begin                       
            if (cHsync < 96) begin                 
                lectura <= 0;
                bloque <=0;
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync < 144) begin
                Hsync <= 1;
                enableFilas <= 0;
                colorOut <= 0;
            end else if (cHsync >= 144 && cHsync < 784) begin                
                if (bloque % 80 == 0) begin
                    cuentaBloque <= cuentaBloque + 1;
                end if (CuentaFilas%60==0 && enableFilas == 0 && CuentaFilas != 0) begin
                    cambioColor <= cambioColor + 8;
                    enableFilas <= 1;                    
                end
                lectura <= cuentaBloque + cambioColor; 
                colorOut <= colorMemory;
                bloque <= bloque + 1;
            end else if (cHsync >= 784 && cHsync < 799) begin
                colorOut <= 0;
            end else if (cHsync == 799)begin
                colorOut <= 0;
                cHsync <= 0;
                cVsync <= cVsync + 1;
                CuentaFilas <= CuentaFilas + 1;
            end            
        end else if (cVsync >= 511 && cVsync < 520) begin            
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync < 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 799)begin
                colorOut <= 0;
                cHsync <= 0;                
                cVsync <= cVsync + 1;
                
            end
        end else if (cVsync == 520) begin                        
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync < 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 799)begin
                colorOut <= 0;                
                cHsync <= 0;                
                cVsync <= 0;
            end
        end            
    end
end

RAM_SINGLE_READ_PORT # (12, 6, 8*8) VideoMemory(
    .Clock (clk2),
    .iWriteEnable (control),
    .iReadAddress (lectura),
    .iWriteAddress (posicionProce),//2^6 6bits
    .iDataIn (RGB),
    .oDataOut (colorMemory)
);   
endmodule     