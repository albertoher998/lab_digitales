module Sumador(
		     input 	in_0,
		     input 	in_1,
		     input 	reset,
			 input clk,
		     input 	ci,
		     output reg co,
		     output reg salida
		     );
   always @(*)begin
      if(reset == 0)begin
	 co = 0;
	 salida = 0;
      end
      else begin
	 {co, salida} = (in_0 + in_1 + ci);
      end
   end
   
endmodule