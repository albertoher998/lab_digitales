`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	output           trap,
	output reg [7:0] anodo,
	output reg [7:0] catodo,
	output reg       enable);
    
	reg [31:0] out_byte;
	reg [15:0] numero14b;
	reg iniciar;
	wire listo;
	wire [31:0] multiplicacion16b;
	wire [3:0] unidades;
	wire [3:0] decenas;
	wire [3:0] centenas;
	wire [3:0] miles;
	wire [3:0] dmiles;
	wire [3:0] cmiles;
	wire [3:0] millones;
	wire [3:0] dmillones;	
	wire [7:0] segmentos_dig0, segmentos_dig1, segmentos_dig2, segmentos_dig3, segmentos_dig4, segmentos_dig5, segmentos_dig6, segmentos_dig7; 
	reg [18:0] contador;
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;

	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			if(mem_la_read && mem_la_addr == 32'h0FFF_FFF8)begin
	    		mem_rdata <= multiplicacion16b;
			end	
			if(mem_la_read && mem_la_addr == 32'h0FFF_FFFC)begin
	    		mem_rdata <= listo;
	 		end
			mem_ready <= 1;
			enable <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				enable <= 1;
				out_byte <= mem_la_wdata;
			end
			if (mem_la_write && mem_la_addr == 32'h0FFFFFF0) begin
				enable <= 1;
				numero14b <= mem_la_wdata;
			end
			if (mem_la_write && mem_la_addr == 32'h0FFFFFF4) begin
				enable <= 1;
				iniciar <= mem_la_wdata;
			end			
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			enable <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					enable <= 1;
					out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate
	
	wire a;
	// always@(*) begin
	// 	if (resetn == 0) begin
	// 		numero14b <= 0;
	// 		numero24b <= 0;
	// 		out_byte <= 0;
	// 	end 
	// end	
	// ArrayMultiplierGEN mul16B(
	// 		.in16_0 (numero14b),
	// 		.in16_1 (numero24b),
	// 		.clk (clk),
	// 		.reset (resetn),
	// 		.out16 (multiplicacion16b)
	// 		);
	factorial factorial_(
		.clk (clk),
		.resetn (resetn),
		.numero_in (numero14b),
		.inicio (iniciar),
		.salida (multiplicacion16b),
		.listo (listo)
	);
	BinarioBCD binBCD(
		.numbin (out_byte),
		.clk (clk),
		.unidades (unidades),
		.decenas (decenas),
		.centenas (centenas),
		.miles (miles),
		.dmiles (dmiles),
		.cmiles (cmiles),
		.millones (millones),
		.dmillones (dmillones)
	);
	BCDseg primerDigito(
		.bcd (unidades),
		.clk (clk),
		.seg (segmentos_dig0)
	);

	BCDseg segundoDigito(
		.bcd (decenas),
		.clk (clk),
		.seg (segmentos_dig1)
	);
	BCDseg tercerDigito(
		.bcd (centenas),
		.clk (clk),
		.seg (segmentos_dig2)
	);
	BCDseg cuartoDigito(
		.bcd (miles),
		.clk (clk),
		.seg (segmentos_dig3)
	);
	BCDseg quintoDigito(
		.bcd (dmiles),
		.clk (clk),
		.seg (segmentos_dig4)
	);
	BCDseg sextoDigito(
		.bcd (cmiles),
		.clk (clk),
		.seg (segmentos_dig5)
	);
	BCDseg setimoDigito(
		.bcd (millones),
		.clk (clk),
		.seg (segmentos_dig6)
	);
	BCDseg octavoDigito(
		.bcd (dmillones),
		.clk (clk),
		.seg (segmentos_dig7)
	);
	
	always @(posedge clk ) begin
		if (resetn == 0) begin
			contador <= 0;
			
			// numero14b <= 0;
			// numero24b <= 0;
			//multiplicacion16b <= 0;
			// unidades <= 0;
			// decenas <= 0;
			// centenas <= 0;
			// miles <= 0;
			// dmiles <= 0;
			// cmiles <= 0;
			// millones <= 0;
			// dmillones <= 0;
			// segmentos_dig0 <= 0;
			// segmentos_dig1 <= 0;
			// segmentos_dig2 <= 0;
			// segmentos_dig3 <= 0;
			// segmentos_dig4 <= 0;
			// segmentos_dig5 <= 0;
			// segmentos_dig6 <= 0;
			// segmentos_dig7 <= 0;
			anodo <= 8'b00000000; 
			catodo <= 8'b00000011;
		end else begin
			
			//unidades
			if (contador < 65536 && contador >= 0 ) begin
				catodo <= segmentos_dig0;
				anodo <= 8'b11111110;
				contador <= contador + 1;
			//decenas	
			end else if (contador < 131072 && contador >= 65536) begin
				catodo <= segmentos_dig1;
				anodo <= 8'b11111101;
				contador <= contador + 1;

			//centenas	
			end else if (contador < 196608 && contador >= 131072) begin
				catodo <= segmentos_dig2;
				anodo <= 8'b11111011;
				contador <= contador + 1;
			//miles
			end else if (contador < 262144 && contador >= 196608) begin
				catodo <= segmentos_dig3;
				anodo <= 8'b11110111;
				contador <= contador + 1;
			//dmiles
			end else if (contador < 327680 && contador >= 262144) begin
				catodo <= segmentos_dig4;
				anodo <= 8'b11101111;
				contador <= contador + 1;
			//cmiles
			end else if (contador < 393216 && contador >= 327680) begin
				catodo <= segmentos_dig5;
				anodo <= 8'b11011111;
				contador <= contador + 1;
			//millones
			end else if (contador < 458752 && contador >= 393216) begin
				catodo <= segmentos_dig6;
				anodo <= 8'b10111111;
				contador <= contador + 1;
			//dmillones
			end else if (contador < 524288 && contador >= 458752) begin
				catodo <= segmentos_dig7;
				anodo <= 8'b01111111;
				contador <= contador + 1;
			end else begin 
				contador <= contador + 1;
			end
		end
	end
	
endmodule
