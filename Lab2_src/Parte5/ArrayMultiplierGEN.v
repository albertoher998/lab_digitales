module ArrayMultiplierGEN(
    input [15:0] in16_0,
    input [15:0] in16_1,
    input reset,
    input clk,
    output [31:0] out16
    
);
    wire [31:0] aCarry [15:0];
    wire [31:0] salidaModulos [15:0];
    wire [14:0] bitMasSig;
    wire [14:0] bitMenosSig;
    // reg Bandera;

    // se define por aparte el primer y ultimo digito de la multiplicacion
    assign out16[0] = in16_0[0] & in16_1[0];
    assign out16[31] = aCarry[14][15]; 
    // Estos son los bits del medio, 1 a 31, se agarran dentro del generate
    // Los bits mas significativos salen de recorrer los ultimos 15 modulos instanciados, el la fila 14
    assign out16[30:16] = bitMasSig;
    // Los Bits menos significativos sale de recoger el valor de los primeros 15 modulos instanciados sobre las primeras 14 filas
    assign out16[15:1] = bitMenosSig;

    



    genvar ROW, COL;
    generate 
        for (ROW = 0; ROW < 15 ; ROW = ROW + 1) begin: MUL_ROW
            assign bitMenosSig[ROW] = salidaModulos[ROW][0];            
            for (COL = 0; COL < 16; COL = COL + 1) begin: MUL_COL 
                if (ROW == 14) begin
                    if(COL > 0)begin                    
                        assign bitMasSig[COL-1] = salidaModulos[14][COL];
                    end
                end
                if (ROW == 0) begin//primera fila
                    if (COL == 15) begin //columna mas a la derecha
                        // primer modulo
                        Sumador sumINI_ROW0_(
                            .in_0 (in16_0[COL] & in16_1[ROW + 1]),
                            .in_1 (1'b0),
                            .clk (clk),
                            .reset (reset),
                            .ci (aCarry[ROW][COL - 1]),
                            .co (aCarry[ROW][COL]),
                            .salida (salidaModulos[ROW][COL])
                        );
                        
                    end else if(COL == 0) begin//columna mas a la izquierda
                        // Ultimo modulo
                        Sumador sumFIN_ROW0_(
                            .in_0 (in16_0[COL] & in16_1[ROW + 1]),
                            .in_1 (in16_0[COL + 1] & in16_1[ROW]),
                            .clk (clk),
                            .reset (reset),
                            .ci (1'b0),                        
                            .co (aCarry[0][0]),
                            .salida (salidaModulos[ROW][COL])
                        );

                    end else begin//resto de columnas 
                        // Modulo comun 
                        Sumador sumNORM_ROW0_(
                            .in_0 (in16_0[COL] & in16_1[ROW + 1]),
                            .in_1 (in16_0[COL + 1] & in16_1[ROW]),
                            .clk (clk),
                            .reset (reset),
                            .ci (aCarry[0][COL - 1]),                        
                            .co(aCarry[0][COL]),
                            .salida(salidaModulos[ROW][COL])
                        );
                    end //fin col15 ROW0
                end else begin
                    if (COL == 15) begin// columna de mas a la derecha
                        Sumador sumINI_ROWn_(
                            .in_0 (in16_0[COL] & in16_1[ROW + 1]),
                            .in_1 (aCarry[ROW - 1][COL]),
                            .clk (clk),
                            .reset (reset),
                            .ci (aCarry[ROW][COL - 1]),                        
                            .co (aCarry[ROW][COL]),
                            .salida (salidaModulos[ROW][COL])
                        );
                    end else if (COL == 0) begin// columna de mas a la izquierda
                        Sumador sumFIN_ROWn_(
                            .in_0 (in16_0[COL] & in16_1[ROW + 1]),
                            .in_1 (salidaModulos[ROW - 1][COL + 1]),
                            .clk (clk),
                            .reset (reset),
                            .ci (1'b0),                        
                            .co (aCarry[ROW][COL]),
                            .salida (salidaModulos[ROW][COL])
                        );
                    end else begin// resto de columnas
                        Sumador sumNORM_ROWn_(
                            .in_0 (in16_0[COL] & in16_1[ROW + 1]),
                            .in_1 (salidaModulos[ROW - 1][COL + 1]),
                            .clk (clk),
                            .reset (reset),
                            .ci (aCarry[ROW][COL - 1]),                        
                            .co (aCarry[ROW][COL]),
                            .salida (salidaModulos[ROW][COL])
                        );
                    end                               
                end
            end        
        end
    endgenerate
endmodule // ArrayMultiplierGEN
                    // if (ROW == 14) begin
                    //     if (COL > 0) begin
                    //         assign out16[COL+15] = salidaModulos[14][0];
                    //     end  
                    // end