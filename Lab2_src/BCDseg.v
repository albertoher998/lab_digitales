module BCDseg(
        input [3:0] bcd,
        input clk,
        output reg [7:0] seg);

   always@(posedge clk) begin
   	
      if(bcd==0)
		seg<=8'b00000011;

      if(bcd==1)
		seg<=8'b10011111;

      if(bcd==2)
		seg<=8'b00100101;

      if(bcd==3)
		seg<=8'b00001101;

      if(bcd==4)
		seg<=8'b10011001;

      if(bcd==5)
		seg<=8'b01001001;

      if(bcd==6)
		seg<=8'b01000001;

      if(bcd==7)
		seg<=8'b00011111;

      if(bcd==8)
		seg<=8'b00000001;

      if(bcd==9)
		seg<=8'b00001001;

      // else
      //    seg=8'b11111111;      

   end 
endmodule 

      
      
     
 
