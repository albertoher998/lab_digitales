module BinarioBCD(
    input [31:0]numbin,
    output reg [3:0] unidades,
	output reg [3:0] decenas,
	output reg [3:0] centenas,
	output reg [3:0] miles,
	output reg [3:0] dmiles,
	output reg [3:0] cmiles,
	output reg [3:0] millones,
	output reg [3:0] dmillones
    
);
    
integer i;
	always @(numbin) begin
		centenas = 0;
		decenas = 0;
		unidades = 0;
		miles = 0;
		dmiles = 0;
		cmiles = 0;
		millones = 0;
		dmillones = 0;
		for ( i = 21; i>=0 ; i = i-1) begin
			if (dmillones >= 5) begin
				dmillones = dmillones + 3;
			end
			if (millones >= 5) begin
				millones = millones + 3;
			end
			if (cmiles >= 5) begin
				cmiles = cmiles + 3;
			end
			if (dmiles >= 5) begin
				dmiles = dmiles + 3;
			end			
			if (miles >= 5) begin
				miles = miles + 3;
			end
			if (centenas >= 5) begin
				centenas = centenas + 3;
			end
			if (decenas >= 5) begin
				decenas = decenas + 3;
			end
			if (unidades >= 5) begin
				unidades = unidades + 3;
			end

			//Shift
			dmillones = dmillones << 1;
			dmillones[0] = millones[3];
			millones = millones << 1;
			millones [0] = cmiles[3];
			cmiles = cmiles << 1;
			cmiles[0] = dmiles[3];
			dmiles = dmiles << 1;
			dmiles[0] = miles[3];
			miles = miles << 1;
			miles[0] = centenas[3];
			centenas = centenas << 1;
			centenas[0] = decenas[3];
			decenas = decenas << 1;
			decenas[0] = unidades[3];
			unidades = unidades << 1;
			unidades[0] = numbin[i];
		end
	end
endmodule // BinarioBCD