#include <stdint.h>

static void putuint10(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}

static void putuint00(uint32_t j) {
	*((volatile uint32_t *)0x0FFFFFF0) = j;
}

static void putuint04(uint32_t k) {
	*((volatile uint32_t *)0x0FFFFFF4) = k;
}

void main() {
	uint32_t multi = 0;
	uint32_t counter = 0;
	uint32_t num1 = 0;
	uint32_t num0 = 0;
	// const uint32_t limit = 10000;

	while (1) {
		counter = 0;
		num0 = 8;
		num1 = 5;
		putuint00(num0);
		putuint04(num1);
		multi = *((volatile uint32_t *)0x0FFFFFF8);
		putuint10(multi);		
		// while (counter < limit) {
		// 	counter++;
		// }
	}
}
