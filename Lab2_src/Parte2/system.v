`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	output           trap,
	output reg [7:0] anodo,
	output reg [7:0] catodo,
	output reg       enable);
    
	reg [22:0] out_byte;
	reg [3:0] numero14b, numero24b;
	wire [7:0] multiplicacion4b;
	wire [3:0] unidades;
	wire [3:0] decenas;
	wire [3:0] centenas;
	wire [3:0] miles;
	wire [3:0] dmiles;
	wire [3:0] cmiles;
	wire [3:0] millones;
	wire [3:0] dmillones;	
	wire [7:0] segmentos_dig0, segmentos_dig1, segmentos_dig2, segmentos_dig3, segmentos_dig4, segmentos_dig5, segmentos_dig6, segmentos_dig7; 
	reg [13:0] contador;
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;

	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			enable <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				enable <= 1;
				out_byte <= mem_la_wdata;
			end
			if (mem_la_write && mem_la_addr == 32'h0FFFFFF0) begin
				enable <= 1;
				numero14b <= mem_la_wdata;
			end
			if (mem_la_write && mem_la_addr == 32'h0FFFFFF4) begin
				enable <= 1;
				numero24b <= mem_la_wdata;
			end
			if(mem_la_read && mem_la_addr == 32'h0FFF_FFF8)begin
	    		mem_rdata <= multiplicacion4b;
			end	
		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			enable <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					enable <= 1;
					out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate
	

	

	//convertidor binario BCD
	// integer i;
	// always @(out_byte) begin
	// 	centenas = 0;
	// 	decenas = 0;
	// 	unidades = 0;
	// 	miles = 0;
	// 	dmiles = 0;
	// 	cmiles = 0;
	// 	millones = 0;
	// 	dmillones = 0;
	// 	for ( i = 21; i>=0 ; i = i-1) begin
	// 		if (dmillones >= 5) begin
	// 			dmillones = dmillones + 3;
	// 		end
	// 		if (millones >= 5) begin
	// 			millones = millones + 3;
	// 		end
	// 		if (cmiles >= 5) begin
	// 			cmiles = cmiles + 3;
	// 		end
	// 		if (dmiles >= 5) begin
	// 			dmiles = dmiles + 3;
	// 		end			
	// 		if (miles >= 5) begin
	// 			miles = miles + 3;
	// 		end
	// 		if (centenas >= 5) begin
	// 			centenas = centenas + 3;
	// 		end
	// 		if (decenas >= 5) begin
	// 			decenas = decenas + 3;
	// 		end
	// 		if (unidades >= 5) begin
	// 			unidades = unidades + 3;
	// 		end

	// 		//Shift
	// 		dmillones = dmillones << 1;
	// 		dmillones[0] = millones[3];
	// 		millones = millones << 1;
	// 		millones [0] = cmiles[3];
	// 		cmiles = cmiles << 1;
	// 		cmiles[0] = dmiles[3];
	// 		dmiles = dmiles << 1;
	// 		dmiles[0] = miles[3];
	// 		miles = miles << 1;
	// 		miles[0] = centenas[3];
	// 		centenas = centenas << 1;
	// 		centenas[0] = decenas[3];
	// 		decenas = decenas << 1;
	// 		decenas[0] = unidades[3];
	// 		unidades = unidades << 1;
	// 		unidades[0] = out_byte[i];
	// 	end
	// end
	
	arrayMultiplier mul(
		.entrada1 (numero14b),
		.entrada2 (numero24b),
		.salida (multiplicacion4b)
	);
	// BinarioBCD multiplica(
	// 	.numbin (multiplicacion4b),
	// 	.unidades (unidades),
	// 	.decenas (decenas),
	// 	.centenas (centenas),
	// 	.miles (miles),
	// 	.dmiles (dmiles),
	// 	.cmiles (cmiles),
	// 	.millones (millones),
	// 	.dmillones (dmillones)
	// );
	BinarioBCD binBCD(
		.numbin (out_byte),
		.unidades (unidades),
		.decenas (decenas),
		.centenas (centenas),
		.miles (miles),
		.dmiles (dmiles),
		.cmiles (cmiles),
		.millones (millones),
		.dmillones (dmillones)
	);
	BCDseg primerDigito(
		.bcd (unidades),
		.seg (segmentos_dig0)
	);

	BCDseg segundoDigito(
		.bcd (decenas),
		.seg (segmentos_dig1)
	);
	BCDseg tercerDigito(
		.bcd (centenas),
		.seg (segmentos_dig2)
	);
	BCDseg cuartoDigito(
		.bcd (miles),
		.seg (segmentos_dig3)
	);
	BCDseg quintoDigito(
		.bcd (dmiles),
		.seg (segmentos_dig4)
	);
	BCDseg sextoDigito(
		.bcd (cmiles),
		.seg (segmentos_dig5)
	);
	BCDseg setimoDigito(
		.bcd (millones),
		.seg (segmentos_dig6)
	);
	BCDseg octavoDigito(
		.bcd (dmillones),
		.seg (segmentos_dig7)
	);
	
	always @(posedge clk ) begin
		if (resetn == 0) begin
			contador <= 0;
			anodo <= 8'b00000000; 
			catodo <= 8'b00000011;
		end else begin
			
			//unidades
			if (contador < 2048 && contador >= 0 ) begin
				catodo <= segmentos_dig0;
				anodo <= 8'b11111110;
				contador <= contador + 1;
			//decenas	
			end else if (contador < 4096 && contador >= 2048) begin
				catodo <= segmentos_dig1;
				anodo <= 8'b11111101;
				contador <= contador + 1;

			//centenas	
			end else if (contador < 6144 && contador >= 4096) begin
				catodo <= segmentos_dig2;
				anodo <= 8'b11111011;
				contador <= contador + 1;
			//miles
			end else if (contador < 8192 && contador >= 6144) begin
				catodo <= segmentos_dig3;
				anodo <= 8'b11110111;
				contador <= contador + 1;
			//dmiles
			end else if (contador < 10239 && contador >= 8192) begin
				catodo <= segmentos_dig4;
				anodo <= 8'b11101111;
				contador <= contador + 1;
			//cmiles
			end else if (contador < 12287 && contador >= 10239) begin
				catodo <= segmentos_dig5;
				anodo <= 8'b11011111;
				contador <= contador + 1;
			//millones
			end else if (contador < 14335 && contador >= 12287) begin
				catodo <= segmentos_dig6;
				anodo <= 8'b10111111;
				contador <= contador + 1;
			//dmillones
			end else if (contador < 16384 && contador >= 14335) begin
				catodo <= segmentos_dig7;
				anodo <= 8'b01111111;
				contador <= contador + 1;
			end else begin 
				contador <= contador + 1;
			end
		end
	end
	
endmodule
