module arrayMultiplier(
	input		[3:0]	entrada1,
	input		[3:0]	entrada2,	
	output reg	[7:0]	salida
	);

	reg acarreo1, acarreo2, acarreo3, acarreo4, acarreo5, acarreo6, acarreo7, acarreo8,	acarreo9, acarreo10, acarreo11, acarreo12;
    reg r1, r2, r3, r4, r5, r6;

	always @(*) begin
		// salida[0]
		salida[0] = entrada1[0] && entrada2[0];
		
		// salida[1]
		{acarreo1, salida[1]} = (entrada1[1] && entrada2[0]) + (entrada1[0] && entrada2[1]);
		
		// salida[2]
		{acarreo2, r1} = (entrada1[1] && entrada2[1]) + (entrada1[2] && entrada2[0]) + acarreo1;
		{acarreo3, salida[2]} = r1 + (entrada1[0] && entrada2[2]);
		
		// salida[3]
		{acarreo4, r2} = (entrada1[2] && entrada2[1]) + (entrada1[3] && entrada2[0]) + acarreo2;
		{acarreo5, r3} = (entrada1[1] && entrada2[2]) + r2 + acarreo3;
		{acarreo6, salida[3]} = (entrada1[0] && entrada2[3]) + r3;
		
		// salida[4]
		{acarreo7, r4} = (entrada1[3] && entrada2[1]) + 0 + acarreo4;
		{acarreo8, r5} = (entrada1[2] && entrada2[2]) + r4 + acarreo5;
		{acarreo9, salida[4]} = (entrada1[1] && entrada2[3]) + r5 + acarreo6;
		
		// salida[5]
		{acarreo10, r6} = (entrada1[3] && entrada2[2]) + acarreo7 + acarreo8;
		{acarreo11, salida[5]} = (entrada1[2] && entrada2[3]) + r6 + acarreo9;
		
		// salida[6]
		{acarreo12, salida[6]} = (entrada1[3] && entrada2[3]) + acarreo10 + acarreo11;

		// salida[7]
		salida[7] = acarreo12;
	end
endmodule