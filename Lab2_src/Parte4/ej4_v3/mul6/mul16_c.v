
`include "mux16_c.v"
`ifndef mul16_c
`define mul16_c

module mul16_c(
output reg [31:0] out_c,
input [15:0] sel,
input [15:0] data_in,
input reset,
input clk);

reg [17:0] ff_out_mux0;
reg [17:0] ff_out_mux1; 
reg [17:0] ff_out_mux2;
reg [17:0] ff_out_mux3;
reg [17:0] ff_out_mux4; 
reg [17:0] ff_out_mux5;
reg [17:0] ff_out_mux6; 
reg [17:0] ff_out_mux7;
 

wire [17:0] out_mux0;
wire [17:0] out_mux1;
wire [17:0] out_mux2;
wire [17:0] out_mux3;
wire [17:0] out_mux4;
wire [17:0] out_mux5;
wire [17:0] out_mux6;
wire [17:0] out_mux7;

reg [19:0] out_shift1;
reg [21:0] out_shift2;
reg [23:0] out_shift3;
reg [25:0] out_shift4;
reg [27:0] out_shift5;
reg [28:0] out_shift6;
reg [30:0] out_shift7;


mux16_c mux0 (
.out_c          (out_mux0),
.sel            (sel[1:0]),
.data_in        (data_in),
.reset    (reset),
.clk      (clk));

mux16_c mux1 (
.out_c          (out_mux1),
.sel            (sel[3:2]),
.data_in        ( data_in),
.reset          (reset),
.clk            (clk));

mux16_c mux2 (
.out_c          (out_mux2),
.sel            (sel[5:4]),
.data_in        (data_in),
.reset    (reset),
.clk      (clk));

mux16_c mux3 (
.out_c          (out_mux3),
.sel            (sel[7:6]),
.data_in        ( data_in),
.reset          (reset),
.clk            (clk));

mux16_c mux4 (
.out_c          (out_mux4),
.sel            (sel[9:8]),
.data_in        (data_in),
.reset    (reset),
.clk      (clk));

mux16_c mux5 (
.out_c          (out_mux5),
.sel            (sel[11:10]),
.data_in        ( data_in),
.reset          (reset),
.clk            (clk));

mux16_c mux6 (
.out_c          (out_mux6),
.sel            (sel[13:12]),
.data_in        (data_in),
.reset    (reset),
.clk      (clk));

mux16_c mux7 (
.out_c          (out_mux7),
.sel            (sel[15:14]),
.data_in        ( data_in),
.reset          (reset),
.clk            (clk));


always @(*)
begin
    if (reset)
    begin
        
    end
end
always @(posedge clk)
begin
    if(reset)
    begin
        ff_out_mux0 <= out_mux0;
       
        out_shift1 <= out_mux1 <<2;
        out_shift2 <= out_mux2 <<4;
        out_shift3 <=out_mux3 <<6;
        out_shift4 <= out_mux4 <<8;
        out_shift5 <= out_mux5 <<10;
        out_shift6 <=out_mux6<<12;
        out_shift7 <=out_mux7 <<14;

        out_c <= ff_out_mux0+ out_shift1 +out_shift2 +out_shift3;

    end
    else
    begin
       ff_out_mux0 <= 0;
       
        out_shift1 <= 0;
        out_shift2 <= 0;
        out_shift3 <=0;
        out_shift4 <= 0;
        out_shift5 <= 0;
        out_shift6 <=0;
        out_shift7 <=0;

        out_c <= 0;
    end
end



endmodule
`endif