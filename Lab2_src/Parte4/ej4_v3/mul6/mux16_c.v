
`ifndef mux_c
`define mux_c

module mux16_c(
output reg [17:0] out_c,
input [1:0] sel,
input [15:0] data_in,
input reset,
input clk);


reg [17:0] f_out_c;


always @(*) 
begin
    out_c = f_out_c;
    if (reset) 
    begin
        case (sel)
            2'b00: out_c = 0;
            2'b01: out_c = data_in;
            2'b10: out_c = data_in << 1;
            2'b11: out_c = data_in +(data_in<<1);
             
        endcase
    end
end

always @(posedge clk) 
begin
    if (reset)
    begin
        f_out_c <= out_c;
    end
    else
    begin
        f_out_c <= 0;
       
    end
end

endmodule
`endif