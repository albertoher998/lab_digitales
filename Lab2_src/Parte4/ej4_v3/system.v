`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	output           trap,
	output reg [7:0] out_byte,
	output reg       out_byte_en
);
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 1;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 4096;

	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;
	
	
	//wires mul4tb
	wire [10:0] out_c;
	wire [3:0] sel;
	wire [3:0] data_in;

	reg [10:0] out_r;
	reg [3:0] sel_r;
	reg [3:0] data_r;

	//wires mul16tb
	wire [31:0] out_c16;
	wire [15:0] sel_16;
	wire [15:0] data_in16;

	reg [31:0] out_r16;
	reg [15:0] sel_r16;
	reg [15:0] data_r16;



	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			mem_ready <= 1;
			out_byte_en <= 0;
			mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			//mul4
			else
			
			if (mem_la_write && mem_la_addr == 32'h10000000) begin
				out_byte_en <= 1;
				data_r <= mem_la_wdata;
				out_byte <= mem_la_wdata;
			end

			if (mem_la_write && mem_la_addr == 32'h10000004) begin
				out_byte_en <= 1;
				sel_r <= mem_la_wdata;
				out_byte <= mem_la_wdata;
			end

			if (mem_la_read && mem_la_addr == 32'h10000008) begin
				mem_rdata <= out_c;
			end
			

			///mul16
			if (mem_la_write && mem_la_addr == 32'h0FFFFFF0) begin
				out_byte_en <= 1;
				data_r16 <= mem_la_wdata;
				out_byte <= mem_la_wdata;
			end

			if (mem_la_write && mem_la_addr == 32'h0FFFFFF4) begin
				out_byte_en <= 1;
				sel_r16 <= mem_la_wdata;
				out_byte <= mem_la_wdata;
			end

			if (mem_la_read && mem_la_addr == 32'h0FFF_FFF8) begin
				mem_rdata <= out_c16;
			end





		end
	end else begin
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready <= mem_valid && !mem_ready && m_read_en;

			m_read_data <= memory[mem_addr >> 2];
			mem_rdata <= m_read_data;

			out_byte_en <= 0;

			(* parallel_case *)
			case (1)
				mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
					if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
					if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
					if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
					if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
					mem_ready <= 1;
				end
				mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
					out_byte_en <= 1;
					out_byte <= mem_wdata;
					mem_ready <= 1;
				end
			endcase
		end
	end endgenerate


	assign sel = sel_r;
	assign data_in = data_r;
	assign out_c = out_r;

   /* mul4_c mul4(
    .out_c 		(out_c),
	.sel    	(sel),
	.data_in 	(data_in),
	.reset  	(resetn),
	.clk   		(clk)    
    );*/


	assign sel_16 = sel_r16;
	assign data_in16 = data_r16;
	assign out_c16 = out_r16;

	mul16_c mul16(
    .out_c 		(out_c16),
	.sel    	(sel_16),
	.data_in 	(data_in16),
	.reset  	(resetn),
	.clk   		(clk)    
    );
endmodule
