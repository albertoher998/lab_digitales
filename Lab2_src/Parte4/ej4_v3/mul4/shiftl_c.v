`ifndef shiftl_c
`define shiftl_c

module shiftl_c (
output reg[7:0] data_out,
input [5:0] data_in,
input reset,
input clk);

always @(posedge clk) 
begin

    if (reset)
    begin
        data_out <= data_in << 2;
    end
    else
    begin 
        data_out <=0;
    end
end
endmodule
`endif