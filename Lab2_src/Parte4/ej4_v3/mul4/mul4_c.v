`include "mux_c.v"
`include "shiftl_c.v"
`ifndef mul4_c
`define mul4_c

module mul4_c(
output reg [7:0] out_c,
input [3:0] sel,
input [3:0] data_in,
input reset,
input clk);

wire [5:0] out_mux0;
wire [5:0] out_mux1;
wire [7:0] out_shift1;
reg [5:0] ff_out_mux0;
reg [5:0] ff_out_mux1; 




mux_c mux0 (
.out_c          (out_mux0),
.sel            (sel[1:0]),
.data_in        (data_in),
.reset    (reset),
.clk      (clk));

mux_c mux1 (
.out_c          (out_mux1),
.sel            (sel[3:2]),
.data_in        ( data_in),
.reset          (reset),
.clk            (clk));

shiftl_c shift1(
.data_out        (out_shift1),
.data_in         (out_mux1),
.reset           (reset),
.clk             (clk));



always @(posedge clk)
begin
    if(reset)
    begin
        ff_out_mux0 <= out_mux0;
        ff_out_mux1 <= out_mux1;
        out_c <= out_shift1 + ff_out_mux0;

    end
end



endmodule
`endif