#include <stdint.h>

static void putuint(uint32_t i) {
	*((volatile uint32_t *)0x10000000) = i;
}

uint32_t factorial(uint32_t j){
	int k;
	uint32_t factorial = 1;
	for (k=1; k <= j; k++)
	{
		factorial = factorial *k;
	}
	return factorial;
}

void main() {
	uint32_t number_to_display = 0;
	uint32_t counter = 0;
	const uint32_t limit = 100000;

	while (1) {
		counter = 0;
		putuint(number_to_display);
		number_to_display++;
		while (counter < limit) {
			counter++;
		}
	
	uint32_t factorial_1 = factorial(5);
	uint32_t factorial_2 = factorial(6);
	uint32_t factorial_3 = factorial(7);

	}
}
