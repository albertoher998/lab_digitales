`timescale 1 ns / 1 ps

module system (
	input            clk,
	input            resetn,
	output           trap,
	output  [7:0] anodo,
	output  [7:0] catodo,
	output reg       enable);

	
	reg [31:0] out_byte;
	// set this to 0 for better timing but less performance/MHz
	parameter FAST_MEMORY = 0;

	// 4096 32bit words = 16kB memory
	parameter MEM_SIZE = 65536;

	wire mem_valid;
	wire mem_instr;
	wire mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	wire [31:0] _mem_rdata_MP;

	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;

	wire mem_valid_MP;
	wire mem_instr_MP;
	reg mem_ready_MP;
	wire [31:0] mem_addr_MP;
	wire [31:0] mem_wdata_MP;
	wire [3:0] mem_wstrb_MP;
	reg [31:0] mem_rdata_MP;
	wire [20:0] hits;
	wire [20:0] miss;

	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.trap        (trap        ),
		.mem_valid   (mem_valid   ),
		.mem_instr   (mem_instr   ),
		.mem_ready   (mem_ready   ),
		.mem_addr    (mem_addr    ),
		.mem_wdata   (mem_wdata   ),
		.mem_wstrb   (mem_wstrb   ),
		.mem_rdata   (mem_rdata   ),
		.mem_la_read (mem_la_read ),
		.mem_la_write(mem_la_write),
		.mem_la_addr (mem_la_addr ),
		.mem_la_wdata(mem_la_wdata),
		.mem_la_wstrb(mem_la_wstrb)
	);

	reg [31:0] memory [0:MEM_SIZE-1];

`ifdef SYNTHESIS
    initial $readmemh("../firmware/firmware.hex", memory);
`else
	initial $readmemh("firmware.hex", memory);
`endif

	reg [31:0] m_read_data;
	reg m_read_en;

	cache #(2048,16) cache_   (	.clk (clk),
   								.resetn(resetn),
   								.mem_valid(mem_valid),
   								.mem_intr(mem_instr),
   								.mem_ready(mem_ready),
   								.mem_addr(mem_addr),
   								.mem_wdata(mem_wdata),
   								.mem_wstrb(mem_wstrb),
   								.mem_rdata(mem_rdata),
   								.mem_valid_MP(mem_valid_MP),
   								// .mem_instr_MP(mem_instr_MP),
   								.mem_ready_MP(mem_ready_MP),
   								.mem_addr_MP(mem_addr_MP),
   								.mem_wdata_MP(mem_wdata_MP),
   								.mem_wstrb_MP(mem_wstrb_MP),
   								.mem_rdata_MP(mem_rdata_MP),
   								.hits(hits),
   								.miss(miss));

	generate if (FAST_MEMORY) begin
		always @(posedge clk) begin
			// mem_ready <= 1;
			enable <= 0;
			// mem_rdata <= memory[mem_la_addr >> 2];
			if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
				if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
				if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
				if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
				if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
			end
			else
			if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
				enable <= 1;
				out_byte <= mem_la_wdata;
			end
		end
	end else begin
		// Usar Este!!!!!!
		always @(posedge clk) begin
			m_read_en <= 0;
			mem_ready_MP <= mem_valid_MP && !mem_ready_MP && m_read_en;

			m_read_data <= memory[mem_addr_MP >> 2];
			mem_rdata_MP <= m_read_data;

			enable <= 0;

			(* parallel_case *)
			case (1)
				mem_valid_MP && !mem_ready_MP && !mem_wstrb_MP && (mem_addr_MP >> 2) < MEM_SIZE: begin
					m_read_en <= 1;
				end
				mem_valid_MP && !mem_ready_MP && |mem_wstrb_MP && (mem_addr_MP >> 2) < MEM_SIZE: begin
					if (mem_wstrb_MP[0]) memory[mem_addr_MP >> 2][ 7: 0] <= mem_wdata_MP[ 7: 0];
					if (mem_wstrb_MP[1]) memory[mem_addr_MP >> 2][15: 8] <= mem_wdata_MP[15: 8];
					if (mem_wstrb_MP[2]) memory[mem_addr_MP >> 2][23:16] <= mem_wdata_MP[23:16];
					if (mem_wstrb_MP[3]) memory[mem_addr_MP >> 2][31:24] <= mem_wdata_MP[31:24];
					mem_ready_MP <= 1;
				end
				mem_valid_MP && !mem_ready_MP && |mem_wstrb_MP && mem_addr_MP == 32'h1000_0000: begin
					enable <= 1;
					out_byte <= mem_wdata_MP;
					mem_ready_MP <= 1;
				end
			endcase
		end
	end 
	endgenerate
	

	// seg7decimal Seven_Seg(
	// 	.x (out_byte),
	// 	.clk (clk),
	// 	.seg (catodo[6:0]),
	// 	.an (anodo),
	// 	.dp (catodo[7])
	// );

endmodule
