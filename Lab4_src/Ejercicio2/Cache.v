module cache #(parameter CACHE_SIZE=1024, parameter BLOCK_SIZE=8)(//Ambos Bytes(
    input 	      clk,
    input 	      resetn,
    //Registers
    input mem_valid,
    input mem_intr,
    output reg mem_ready,
    input [31:0] mem_addr,
    input [31:0] mem_wdata,
    input [3:0] mem_wstrb,
    output reg [31:0] mem_rdata,
    //Main Memory
    output reg mem_valid_MP,
    // output mem_instr_MP,
    input mem_ready_MP,
    output reg [31:0] mem_addr_MP,
    output reg [31:0] mem_wdata_MP,
    output reg [3:0]  mem_wstrb_MP,
    input [31:0] mem_rdata_MP,
    output reg [20:0] hits,
    output reg [20:0] miss
);

    //Parametros de cache
    parameter WAYS = 2;
    parameter WORD_SIZE = 4;
    parameter WORD_BIT_SIZE = 8*WORD_SIZE;
    parameter NUM_BLOCK  = CACHE_SIZE/BLOCK_SIZE; 
    parameter OFFSET_SIZE = $clog2(BLOCK_SIZE);    
    parameter WORDS_BLOCK = BLOCK_SIZE/WORD_SIZE;
    parameter SETS = NUM_BLOCK/WAYS;
    parameter INDEX_SIZE = $clog2(SETS);
    parameter TAG_SIZE = 32-INDEX_SIZE-OFFSET_SIZE;
    parameter CACHE_TAG_SIZE = 1+1+1+TAG_SIZE; // db + valid + LRU + tag

    //States
    parameter IDLE = 1;
    parameter READ = 2;
    parameter WRITE = 4;
    parameter READ_MISS = 8;
    parameter WRITE_MISS = 16;
    parameter MEM_ACCESS = 32; 
    parameter MEM_WRITE = 64;  
    parameter WRITE_BACK = 128;

    //Arreglo de entradas de cache
    //DB+VB+LRU+TAG
    reg [CACHE_TAG_SIZE-1:0] reg_address[SETS-1:0][WAYS-1:0];
    
    //DATOS
    //Arreglo de datos en cada entrada
    reg [31:0] cache_data[WAYS-1:0][WORDS_BLOCK-1:0][SETS-1:0];
    wire [TAG_SIZE-1:0] tag;
    wire [INDEX_SIZE-1:0] index;
    wire [OFFSET_SIZE-1:0] offset;
    reg dato0Valid, ReadWrite_Flag, MemoryRW;
    reg [7:0] STATE;
    reg [31:0] temporal_address;
    reg [OFFSET_SIZE-1:0] offset_temp;
    reg [OFFSET_SIZE:0] blockCounter;
    reg read_miss_flag;
    reg way_temp;
    reg [31:0] temporal_address_W;
    reg offset_temp_w;
    reg [OFFSET_SIZE:0] counter_W;
    
    
    assign tag = mem_addr[31:32-TAG_SIZE];
    assign index = mem_addr[INDEX_SIZE+OFFSET_SIZE-1:OFFSET_SIZE];
    assign offset = mem_addr[OFFSET_SIZE-1:0];

    integer i,j;
    always @(posedge clk) begin
        if (resetn == 0) begin
            hits <= 0;
            miss <= 0;
            STATE <= IDLE;
             for (i = 0; i < SETS; i = i + 1) begin
                for (j = 0; j < WAYS; j = j + 1) begin
                    reg_address[i][j] <= 0;
                    // reg_address[i][j][CACHE_TAG_SIZE-3] <= 1;
                end
            end
        end else begin 
            case (STATE)
                IDLE: begin
                    mem_ready <= 0;
                    if (mem_valid)  begin
                        if (|mem_wstrb)  begin
                            STATE <= WRITE;
                        end
                        if (!mem_wstrb) begin
                            STATE <= READ;
                        end
                    end
                end 
                READ: begin
                read_miss_flag <= 0;
                    if (reg_address[index][0][TAG_SIZE-1:0] == tag && reg_address[index][0][CACHE_TAG_SIZE-2]) begin
                        // reg [31:0] cache_data[WAYS-1:0][WORDS_BLOCK-1:0][SETS-1:0];
                        mem_ready <= 1;
	                    mem_rdata <= cache_data[0][offset>>2][index];   
                        reg_address[index][0][CACHE_TAG_SIZE-3] <= 0;
                        reg_address[index][1][CACHE_TAG_SIZE-3] <= 1;
                        STATE <= IDLE;
                        hits <= hits +1;
                    end else if (reg_address[index][1][TAG_SIZE-1:0] == tag && reg_address[index][1][CACHE_TAG_SIZE-2]) begin
                        mem_ready <= 1;
                        mem_rdata <= cache_data[1][offset>>2][index];                      
                        reg_address[index][0][CACHE_TAG_SIZE-3] <= 1;
                        reg_address[index][1][CACHE_TAG_SIZE-3] <= 0;
                        STATE <= IDLE;
                        hits <= hits +1;
                    end
                    else begin //Miss
                        
                        miss <= miss + 1;
                        temporal_address <= mem_addr & (32'hFFFFFFFF<<(3));
                        offset_temp <= 0;
                        blockCounter <= 0;
                        offset_temp_w <= 0;
                        counter_W <= 0;
                        if(!reg_address[index][0][CACHE_TAG_SIZE-2])begin
                            
                            reg_address[index][0][CACHE_TAG_SIZE-2] <= 1;
                            reg_address[index][0][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 0;		 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 1;	 
                            reg_address[index][0][CACHE_TAG_SIZE-1] <= 0;
                            way_temp <= 0;
                            STATE <= READ_MISS;
                        end else if (!reg_address[index][1][CACHE_TAG_SIZE-2]) begin
                            reg_address[index][1][CACHE_TAG_SIZE-1] <= 1'b1;
                            reg_address[index][1][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 1;		 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 0;	 
                            reg_address[index][1][CACHE_TAG_SIZE-2] <= 0;
                            way_temp <= 1;
                            STATE <= READ_MISS;
                        end else if (reg_address[index][0][CACHE_TAG_SIZE-3]) begin
                            if (reg_address[index][0][CACHE_TAG_SIZE-1]) begin
                                STATE <= WRITE_BACK;
                                temporal_address_W <= ((reg_address[index][0][TAG_SIZE-1]<<(INDEX_SIZE))+index)<<3;
                            end else begin
                                STATE <= READ_MISS;
                            end
                            reg_address[index][0][CACHE_TAG_SIZE-1] <= 1'b1;
                            reg_address[index][0][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 0;		 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 1;	 
                            reg_address[index][0][CACHE_TAG_SIZE-2] <= 0;
                            way_temp <= 0;
                        end else if (reg_address[index][1][CACHE_TAG_SIZE-3]) begin
                            if (reg_address[index][1][CACHE_TAG_SIZE-1]) begin
                                STATE <= WRITE_BACK;
                                temporal_address_W <= ((reg_address[index][1][TAG_SIZE-1:0]<<(INDEX_SIZE))+index)<<3;
                            end else begin
                                STATE <= READ_MISS;
                            end
                            reg_address[index][1][CACHE_TAG_SIZE-1] <= 1'b1;
                            reg_address[index][1][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 1; 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 0; 
                            reg_address[index][1][CACHE_TAG_SIZE-2] <= 0;
                            way_temp <= 1;
                        end
                    end
                end
                WRITE: begin
                read_miss_flag <= 1;
                    if(reg_address[index][0][TAG_SIZE-1:0] == tag && reg_address[index][0][CACHE_TAG_SIZE-2])begin
                        cache_data[0][offset][index] <= mem_wdata;
                        mem_ready <= 1;
                        reg_address[index][0][CACHE_TAG_SIZE-3] <= 0;
                        reg_address[index][1][CACHE_TAG_SIZE-3] <= 1;
                        reg_address[index][0][CACHE_TAG_SIZE-1] <= 1;
                        STATE <= IDLE;
                        hits <= hits + 1;
                    end else if (reg_address[index][1][TAG_SIZE-1] == tag && reg_address[index][1][CACHE_TAG_SIZE-2]) begin
                        cache_data[1][offset][index] <= mem_wdata;
                        mem_ready <= 1;
                        reg_address[index][1][CACHE_TAG_SIZE-3] <= 0;
                        reg_address[index][0][CACHE_TAG_SIZE-3] <= 1;
                        reg_address[index][1][CACHE_TAG_SIZE-1] <= 1;
                        STATE <= IDLE;
                        hits <= hits + 1;

                    end else begin
                        miss <= miss + 1;
                        
                        temporal_address <= mem_addr & (32'hFFFFFFFF<<(3));
                        offset_temp <= 0;
                        blockCounter <= 0;
                        offset_temp_w <= 0;
                        counter_W <= 0;
                        if (!reg_address[index][0][CACHE_TAG_SIZE-2]) begin
                            reg_address[index][0][CACHE_TAG_SIZE-2] <= 1'b1;
                            reg_address[index][0][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 0;		 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 1;	 
                            reg_address[index][0][CACHE_TAG_SIZE-1] <= 1;
                            way_temp <= 0;
                            STATE <= WRITE_MISS;
                        end else if (!reg_address[index][1][CACHE_TAG_SIZE-2]) begin
                            reg_address[index][1][CACHE_TAG_SIZE-1] <= 1'b1;
                            reg_address[index][1][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 1;		 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 0;	 
                            reg_address[index][1][CACHE_TAG_SIZE-1] <= 1;
                            way_temp <= 1;
                            STATE <= WRITE_MISS;
                        end else if (reg_address[index][0][CACHE_TAG_SIZE-3]) begin
                            if (reg_address[index][0][CACHE_TAG_SIZE-1]) begin
                                STATE <= WRITE_BACK;
                                temporal_address_W = ((reg_address[index][0][TAG_SIZE-1:0]<<(INDEX_SIZE))+index)<<3;
                            end else begin
                                STATE <= WRITE_MISS;
                            end
                            reg_address[index][0][CACHE_TAG_SIZE-1] <= 1'b1;
                            reg_address[index][0][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 0;		 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 1; 
                            reg_address[index][0][CACHE_TAG_SIZE-1] <= 1;
                            way_temp <= 0;
                        end else if (reg_address[index][1][CACHE_TAG_SIZE-3]) begin
                            if (reg_address[index][1][CACHE_TAG_SIZE-1]) begin
                                STATE <= WRITE_BACK;
                                temporal_address_W <= ((reg_address[index][1][TAG_SIZE-1:0]<<(INDEX_SIZE))+index)<<3;
                            end else begin
                                STATE <= WRITE_MISS;
                            end
                            reg_address[index][1][CACHE_TAG_SIZE-1] <= 1'b1;
                            reg_address[index][1][TAG_SIZE-1:0] <= tag;
                            reg_address[index][0][CACHE_TAG_SIZE-3] <= 1;	 
                            reg_address[index][1][CACHE_TAG_SIZE-3] <= 0;
                            reg_address[index][1][CACHE_TAG_SIZE-1] <= 1;
                            way_temp <= 1;                            
                        end
                    end
                end
                READ_MISS: begin
                    mem_valid_MP <= 0;
                    if (blockCounter < WORDS_BLOCK) begin
                        STATE <= MEM_ACCESS;
                    end else begin
                        mem_ready <= 1;
                        // reg [31:0] cache_data[WAYS-1:0][WORDS_BLOCK-1:0][SETS-1:0];
                        mem_rdata <= cache_data[way_temp][offset>>2][index];
                        STATE <= IDLE;
                    end                    
                end
                WRITE_MISS: begin
                    if (blockCounter < WORDS_BLOCK) begin
                        STATE <= MEM_ACCESS;
                    end else begin
                        cache_data[way_temp][offset>>2][index] <= mem_wdata;
                        mem_ready <= 1;
                        STATE <= IDLE;
                    end
                end
                MEM_ACCESS: begin
                    mem_valid_MP <= 1;
                    mem_addr_MP <= temporal_address;
                    mem_wdata_MP <= mem_wdata;
                    mem_wstrb_MP <= 0;
                    if (mem_ready_MP) begin
                        cache_data[way_temp][offset_temp][index] <= mem_ready_MP;
                        blockCounter <= blockCounter + 1;
                        offset_temp <= offset_temp + 1;
                        temporal_address <= temporal_address + 4;
                        if (read_miss_flag) begin
                            STATE <= WRITE_MISS;
                        end else begin
                            STATE <= READ_MISS;
                        end
                    end
                end
                MEM_WRITE: begin
                    mem_valid_MP <= 1;
                    mem_addr_MP <= temporal_address_W;
                    mem_wdata_MP <= cache_data[way_temp][offset_temp_w][index];
                    mem_wstrb_MP <= 4'b1111;
                    if (mem_ready_MP) begin
                        counter_W <= counter_W + 1;
                        offset_temp_w <= offset_temp_w + 1;
                        STATE <= WRITE_BACK;
                    end                
                end
                WRITE_BACK: begin
                    mem_valid_MP <= 0;
                    if (counter_W < WORDS_BLOCK) begin
                        temporal_address_W <= temporal_address_W + 4;
                        STATE <= MEM_WRITE;
                    end else if (read_miss_flag) begin
                        STATE <= WRITE_MISS;
                    end else begin
                        STATE <= READ_MISS;
                    end
                end
                default: 
                STATE <= IDLE;
            endcase
        end
    end
   
   
   
endmodule