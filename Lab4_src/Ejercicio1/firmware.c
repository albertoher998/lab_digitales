 #include <stdint.h>

 static void putuint(uint32_t info, uint32_t escritura) {
 	*((volatile uint32_t *)escritura) = info;
 }

 void main() {
 	uint32_t dato = 0;
 	uint32_t dato2 = 0;
 	uint32_t posicion = 0x4000;
 	uint32_t lectura = 0x4000;
 	uint32_t impar5 [5] = {0,0,0,0,0};
 	uint32_t i = 0;
 	uint32_t k = 0;
 	uint32_t enable = 0;
 	uint32_t contImpresion = 0;
 	while (1) {
		
 		while (posicion <= 0xFFF8){
 			putuint(posicion+8, posicion);
 			putuint(dato, posicion+4);						
 			posicion = posicion + 8;
 			dato++;			
 		}
		
		
 		while(lectura <= 0xFFF8){
 			dato2 = *((volatile uint32_t *)(lectura + 4));
 			if (dato2%2 == 1){
 				impar5[i] = dato2;
 				i++;
 				if (i == 5){
 					i = 0;
 				}				
 			}		
 			lectura = *((volatile uint32_t *)lectura);			
 		}
 		contImpresion++;		
 		if (contImpresion == 2000000){			
 			contImpresion = 0;
 			putuint(impar5[k], 0x10000000);
 			k++;
 			if (k == 5){
 				k = 0;
 			}
 		}	
 	}
 }


