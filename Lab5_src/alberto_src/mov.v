module mov(
    input clk,
    input [9:0] posbarraiy,
    input [9:0] posbarrady,
    input [4:0] Brstn,
    input resetn,
    output reg [15:0]counterP2,
    output reg [15:0] counterP1,
    output reg [13:0] posBola, // cambiar dependiendo de los limites de la pantalla   
    output reg [11:0] colorBola,
    output reg Estripo
    );
    reg clk2;   
    reg [18:0] temp;   
    reg dirx;
    reg diry;
    reg [6:0] posx;
    reg [6:0] posy;
    reg [25:0]counter;
    reg resetC;
    

    always @(posedge clk) begin
        if (resetn == 0) begin
            temp <= 0;
            counter <= 0;
            colorBola <= 'h0FF;
            // counterP1 = 0;
            // counterP2 = 0;
            resetC <= 1;
        end else begin
           temp<=temp+1;
            counter <= counter + 1;
            if (Brstn[4] == 1) begin
                resetC <= 1;
//                Estripo <= 1;
            end else begin
                resetC <= 0;
            end
            if (counter == 12108863) begin
                colorBola <= colorBola + 255;
                        
            end
            clk2<=temp[18]; 
        end   
    end
 
    always @(posedge clk2) begin
        if (resetn == 0) begin
            counterP1 = 0;
            counterP2 = 0;
            Estripo <= 0;
            posx = 40;
            posy = 40;
            dirx = 0;
            diry = 0;
        end else begin
            posBola = 80*posy + posx;
            if (resetC == 1)begin 
                counterP1 = 0;
                counterP2 = 0;
                posx = 40;
                posy = 40; 
            end else begin
                if (dirx == 0) begin
                    if (posx == 1) begin
                        counterP2 = counterP2 + 1;
                        Estripo <= 1;
                    end
                end else if (dirx == 1) begin
                    if (posx == 78) begin
                        counterP1 = counterP1 + 1;
                        Estripo <= 0;
                    end
                end    
            end
            
            // Golpea diagonal abajo derecha
            

            //movimiento arriba y derecha
            if (dirx==0 & diry==0) begin
                
                posx=posx+1;
                posy=posy+1;
                if (posx>=79) dirx=1;
                else dirx=0;
                if (posy>=79) diry=1;
                else diry=0;
    
                // Golpe Con la Paleta
                if(posx == 4 & posy>=posbarraiy - 5 & posy<=posbarraiy + 5) 
                begin
                // colorBola <= colorBola + 10;
                dirx=0;
                diry=0;
                end
                else if(posx == 75 & posy>=posbarrady - 5 & posy<=posbarrady + 5)
                begin
                // colorBola <= colorBola + 10;
                dirx=1;
                diry=0;
                end
            end
    
            //movimiento arriba e izquierda
            else if (dirx==1 & diry==0) begin
                posx=posx-1;
                posy=posy+1;
                if (posx <=0) dirx=0;
                else dirx=1;
                if (posy>=79) diry=1;
                else diry=0;
    
                if(posx == 4 & posy>=posbarraiy - 5 & posy<=posbarraiy + 5) 
                begin
                // colorBola <= colorBola + 10;
                dirx=0;
                diry=0;
                end
                else if(posx == 75 & posy>=posbarrady - 5 & posy<=posbarrady + 5)
                begin
                // colorBola <= colorBola + 10;
                dirx=1;
                diry=0;
                end
    
            end
            //movimiento abajo y derecha
            else if (dirx==0 & diry==1) begin
                posx=posx+1;
                posy=posy-1;
                if (posx>=79) dirx=1;
                else dirx=0;
                if (posy<=1) diry=0;
                else diry=1;
    
                if(posx == 4 & posy>=posbarraiy - 5 & posy<=posbarraiy + 5) 
                begin
                // colorBola <= colorBola + 10;
                dirx=0;
                diry=1;
                end
                else if(posx == 75 & posy>=posbarrady - 5 & posy<=posbarrady + 5)
                begin
                // colorBola <= colorBola + 10;
                dirx=1;
                diry=1;
                end
    
            end
            // movimiento abajo e izquierda
            if (dirx==1 & diry==1) begin
                posx=posx-1;
                posy=posy-1;
                if (posx<=0) dirx=0;
                else dirx=1;
                if (posy<=1) diry=0;
                else diry=1;
    
                if(posx == 4 & posy>=posbarraiy - 5 & posy<=posbarraiy + 5) 
                begin
                // colorBola <= colorBola + 10;
                dirx=0;
                diry=1;
                end
                else if(posx == 75 & posy>=posbarrady - 5 & posy<=posbarrady + 5)
                begin
                // colorBola <= colorBola + 10;
                dirx=1;
                diry=1;
                end
            end        
        end
    end
        
 
endmodule
