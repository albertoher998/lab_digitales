module Debouncer(
    input clk25M,
    input ButtonD,
    output reg byte,

    output reg ButtonOut
);    
    reg ButtonF1, ButtonF2, ButtonF3, ButtonF4, ButtonF5, ButtonF6, enable, ande;
	reg [21:0] cont;
    reg [9:0] later;
    reg enable4;
	// assign reset_cont = ButtonF1^ButtonF2;
    always@(posedge clk25M)begin
		ButtonOut <= 0;
        byte <= 0;
        
        ButtonF1 <= ButtonD;       
        ButtonF2 <= ButtonF1;
        ButtonF3 <= ButtonF2;
        ButtonF4 <= ButtonF3;
        ButtonF5 <= ButtonF4;
        ButtonF6 <= ButtonF5;
        ande <= ButtonF1 & ButtonF2 & ButtonF3 & ButtonF4 & ButtonF5 & ButtonF6;
        
    	
    	
        if(ande == 1)begin
	        enable <= 1;
        end
        if (later == 1020) begin
            enable4 <= 1;
        end 
        if (later == 510) begin
            enable4 <= 0;
        end
        if (enable4 == 1) begin
            if(cont == 819430 && enable == 1)begin
                later <= later + 1;
                ButtonOut <= ande;
                enable <= 0;
                byte <= 1;	 
                cont <= 0;		
            end else begin
                cont <= cont + 1;
            end
        end else begin
            if(cont == 419430 && enable == 1)begin
                later <= later + 1;
                ButtonOut <= ande;
                enable <= 0;
                byte <= 1;	 
                cont <= 0;		
            end else begin
                cont <= cont + 1;
            end
        end
		
      
    end
endmodule