module goodVGAaccsess(
    input clk,
    input clk2,
    input resetn,
    input control,
    input [31:0] posicion,
    input clk_ps2,
    input data_ps2,
    input [11:0]RGB,
    input [4:0] button,

    output reg Hsync,
    output reg Vsync,
    output [15:0]counterP1,
    output [15:0]counterP2,
    output [31:0]teclado,    
    output Estripo,
    output reg [11:0]colorOut       
);
wire Sclk;

reg [13:0] lectura, cambioColor;
wire [13:0] posicionButton, posicionTeclado;
reg [9:0] CuentaFilas;
reg [13:0] posicionProce;
wire [6:0] filas, columnas;
reg [9:0] cuentaBloque;
reg [9:0] cVsync, cHsync, bloque;
wire [11:0] colorMemory, rojo, azul;
reg enableFilas;
wire clkTeclado;
wire [6:0] BarrD, BaddI;
wire [13:0] xyBOLA;
wire [11:0] colorBola;
reg enableCols;
// wire Estripo;
// wire [31:0] teclado;

assign columnas = posicion[6:0];
assign filas = posicion[22:16];

always @(*) begin
    if (resetn == 0) begin
        posicionProce = 0;
    end
    posicionProce = filas * 80 + columnas;
end
always @(posedge clk ) begin
    if (resetn == 0) begin
        CuentaFilas <= 0;
        cuentaBloque <= 0;
        cambioColor <= 0;
        colorOut <= 0;
        enableFilas <= 0;
        enableCols <=0;
        lectura <= 0;
        bloque <=0;
        cHsync <= 0;
        cVsync <= 0;
        Hsync <= 0;
        Vsync <= 0;
    end else begin 
        cHsync <= cHsync + 1;
        if (cVsync < 2) begin 
            CuentaFilas <= 0;
            cambioColor <= 0;
            
            Vsync <=0;           
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync <= 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 800)begin
                colorOut <= 0;
                cHsync <= 0;
                cVsync <= cVsync + 1;
            end            
        end else if (cVsync >= 2 && cVsync < 31) begin  
            Vsync <= 1;          
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync <= 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 800)begin
                colorOut <= 0;
                cHsync <= 0;
                cVsync <= cVsync + 1;
            end            
        end else if (cVsync >= 31 && cVsync < 511) begin                       
            if (cHsync < 96) begin                 
                lectura <= 0;
                bloque <=0;
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync < 144) begin
                Hsync <= 1;
                enableFilas <= 0;
                colorOut <= 0;
            end else if (cHsync >= 144 && cHsync < 784) begin  
                // Tamano Bloque   
                if (enableCols == 0)begin 
                    //nada
                    enableCols <= 1;
                end else if (bloque % 8 == 0) begin
                    cuentaBloque <= cuentaBloque + 1;
                    
                    if (cuentaBloque == 79) begin
                        cuentaBloque <= 0;
                    end
                end if (CuentaFilas%6==0 && enableFilas == 0 && CuentaFilas != 0) begin
                    cambioColor <= cambioColor + 80;
                    // if (cambioColor == 6400) begin
                    //     cambioColor <= 0;
                    // end
                    enableFilas <= 1;                    
                end
                lectura <= cuentaBloque + cambioColor; 
                if (lectura == xyBOLA) begin
                    colorOut <= colorBola;
                end else if (lectura == posicionButton) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton+80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton+2*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton+3*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton+4*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton+5*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton-80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton-2*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton-3*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton-4*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionButton-5*80) begin
                    colorOut <= rojo;
                end else if (lectura == posicionTeclado) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado+80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado+2*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado+3*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado+4*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado+5*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado-80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado-2*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado-3*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado-4*80) begin
                    colorOut <= azul;
                end else if (lectura == posicionTeclado-5*80) begin
                    colorOut <= azul;
                end else begin
                    colorOut <= colorMemory;
                end                                
                bloque <= bloque + 1;
            end else if (cHsync >= 784 && cHsync <= 799) begin
                colorOut <= 0;
            end else if (cHsync == 800)begin
                colorOut <= 0;
                cHsync <= 0;
                cVsync <= cVsync + 1;
                CuentaFilas <= CuentaFilas + 1;
            end            
        end else if (cVsync >= 511 && cVsync <= 520) begin            
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync <= 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 800)begin
                colorOut <= 0;
                cHsync <= 0;                
                cVsync <= cVsync + 1;
                
            end
        end else if (cVsync == 521) begin                        
            if (cHsync < 96) begin                
                Hsync <= 0;
                colorOut <= 0;
            end else if (cHsync >= 96 && cHsync <= 799) begin
                Hsync <= 1;
                colorOut <= 0;            
            end else if (cHsync == 800)begin
                colorOut <= 0;                
                cHsync <= 0;                
                cVsync <= 0;
            end
        end            
    end
end

// SuperClk #(256) Supclock(
//         .clk (clk),
//         .reset (resetn),

//         .clk_div (Sclk)
// );

PS2Receiver Iteclado_(
    .clk (clk),
    .kclk (clk_ps2),
    .kdata (data_ps2),

    .keycodeout (teclado),
    .kclkf (clkTeclado)
);
mov Bola(
    .clk(clk),

    .posbarraiy (BaddI),
    .posbarrady (BarrD),
    .Brstn (button),
    .resetn (resetn),
    

    .counterP1 (counterP1),
    .counterP2 (counterP2),    

    .colorBola (colorBola),
    .posBola (xyBOLA),
    .Estripo (Estripo)

);  
boardAccsess pintarPaleta1(
    .button (button),
    // .teclado (teclado),
    .clk (clk),
    .clkTeclado(clkTeclado),
    .resetn (resetn),

    .filas (BarrD),
    .posicion (posicionButton),
    .rojo (rojo)
);
boardAccsessTec pintarPaleta2(
    .teclado (teclado),
    .clk (clk),
    .clkTeclado (clkTeclado),
    .resetn (resetn),

    .filas (BaddI),
    .posicion (posicionTeclado),
    .azul (azul)
);

RAM_SINGLE_READ_PORT # (12, 14, 80*81) VideoMemory(
    .Clock (clk2),
    .iWriteEnable (control),
    .iReadAddress (lectura),
    .iWriteAddress (posicionProce),//2^14 6bits
    .iDataIn (RGB),
    .oDataOut (colorMemory)
); 

endmodule     