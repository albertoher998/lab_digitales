module DebouncerTec(
    input clk25M,
    input [31:0]ButtonD,    

    output reg[31:0] ButtonOut
);    
    reg [31:0] ButtonF1, ButtonF2, ButtonF3, ButtonF4, ButtonF5, ButtonF6, ande;
    reg enable;
	reg [21:0] cont;
    reg [9:0] later;
    reg enable4;
	// assign reset_cont = ButtonF1^ButtonF2;
    always@(posedge clk25M)begin
		// ButtonOut <= 'hF0;        
        
        ButtonF1 <= ButtonD;       
        ButtonF2 <= ButtonF1;
        ButtonF3 <= ButtonF2;
        ButtonF4 <= ButtonF3;
        ButtonF5 <= ButtonF4;
        ButtonF6 <= ButtonF5;       
    	
    	
        if (later == 1020) begin
            enable4 <= 1;
        end
        if (later == 510) begin
            enable4 <= 0;
        end
        if (enable4 == 1) begin
            if(cont == 819430)begin
                later <= later + 1;
                ButtonOut <= ButtonF6;
                enable <= 0;            	 
                cont <= 0;		
            end else begin
                cont <= cont + 1;
                ButtonOut <= 'hF0;
            end
        end else begin
            if(cont == 419430)begin
                later <= later + 1;
                ButtonOut <= ButtonF6;
                enable <= 0;            	 
                cont <= 0;		
            end else begin
                cont <= cont + 1;
                ButtonOut <= 'hF0;
            end
        end
		
      
    end
endmodule