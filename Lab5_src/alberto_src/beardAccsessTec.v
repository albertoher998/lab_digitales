module boardAccsessTec(
    // input [4:0] button,
    input [31:0] teclado,
    input clk,
    input clkTeclado,
    input resetn,

    output reg [6:0]filas,
    output reg [13:0]posicion,
    output reg [11:0]azul
);
    wire [31:0] tecLento;
    reg [6:0] columnas;
    wire neg;
    

    DebouncerTec addLatency_(
        .clk25M (clk),
        .ButtonD (teclado),

        .ButtonOut (tecLento)
    );
//    assign neg = ~(button[0]|button[1]|button[2]|button[3]|button[4]);
    always @(posedge clk) begin
        
        if (resetn == 0) begin
            filas <= 40;            
            columnas <= 4;
            azul <= 'h0FF;
            // posicion <= 3204;            
        end else begin
            
            posicion <= 80*filas + columnas;
            if (filas > 73) begin
                filas <= 73;
            end 
            
            if (filas < 6) begin
                filas <= 6;
            end
            
            if ((tecLento[7:0] == 'h75/*'h1D*/ && tecLento[15:8] != 'hF0)) begin
                filas <= filas - 1;                
            end
            if (tecLento[7:0] == 'h72/*'h1B*/ && tecLento[15:8] != 'hF0) begin
                filas <= filas + 1;
            end
            // if (tecLento[7:0] == 'h1C && tecLento[15:8] != 'hF0) begin
            //     columnas <= columnas - 1;
            // end
            // if (tecLento[7:0] == 'h23 && tecLento[15:8] != 'hF0) begin
            //     columnas <= columnas + 1;
            // end
            // if (tecLento[7:0] == 'h29 && tecLento[15:8] != 'hF0) begin
            //     if (azul == 'hF00)begin
            //         azul <= 'h0F0;
            //     end
            //     if (azul == 'h0F0) begin
            //         azul <= 'h00F;
            //     end
            //     if (azul == 'h00F) begin
            //         azul <= 'hF00;
            //     end
            // end
            // // W
            // if (teclado == 'h1D) begin
            //     filas <= filas - 1;  
            // end
            // // S
            // if (teclado == 'h1B) begin
            //     filas <= filas + 1;
            // end
            // // A
            // if (teclado == 'h1C) begin
            //     columnas <= columnas - 1;
            // end
            // // D
            // if (teclado == 'h23) begin
            //     columnas <= columnas + 1;
            // end
        end
    end
endmodule 